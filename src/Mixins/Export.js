import vue from 'vue'
import UserMixins from './UserMixins'
import Format from './Format'
import TrainMixins from './TrainMixins'
import StationMixins from './StationMixins'
import ScheduleMixins from './ScheduleMixins'
import RouteMixins from './RouteMixins'

vue.mixin(UserMixins)
vue.mixin(Format)
vue.mixin(TrainMixins)
vue.mixin(StationMixins)
vue.mixin(ScheduleMixins)
vue.mixin(RouteMixins)
