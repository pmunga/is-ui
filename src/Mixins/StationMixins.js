export default {
  methods: {
    $openStation (stationId) {
      this.$router.push({
        name: 'Station',
        params: {
          station_id: stationId
        }
      })
    }
  },
  computed: {
    $allStations () {
      return this.$store.state.StationStore.all_stations
    },
    $singleStation  () {
      return this.$store.state.StationStore.station
    }
  }
}
