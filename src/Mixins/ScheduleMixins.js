export default {
  methods: {
    $openSchedule (scheduleId) {
      this.$router.push({
        name: 'Schedule',
        params: {
          train_id: scheduleId
        }
      })
    }
  },
  computed: {
    $allSchedules () {
      return this.$store.state.ScheduleStore.all_schedules
    },
    $singleSchedule () {
      return this.$store.state.ScheduleStore.schedule
    },
    $searchResults () {
      return this.$store.state.ScheduleStore.search_results
    }
  }
}
