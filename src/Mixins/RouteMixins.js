export default {
  methods: {
    $openRoute (routeId) {
      this.$router.push({
        name: 'Route',
        params: {
          route_id: routeId
        }
      })
    }
  },
  computed: {
    $allRoutes () {
      return this.$store.state.RouteStore.all_routes
    },
    $singleRoute () {
      return this.$store.state.RouteStore.route
    }
  }
}
