export default {
  data () {
    return {
      textRules: [
        v => !!v || 'This field is required'
      ],
      minDate: this.$moment().format('YYYY-MM-DD')
    }
  },
  methods: {
    $formated_date: function (date) {
      if (date === null) {
        return null
      }
      if (this.$moment.parseZone(date).format('YYYY-MM-DD') === this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return 'Today | ' + this.$moment.parseZone(date).format('DD, MMM YYYY')
      } else {
        return this.$moment(date).format('DD, MMM YYYY')
      }
    },
    $formated_date_time: function (date) {
      if (this.$moment.parseZone(date).format('YYYY-MM-DD') === this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return 'Today | ' + this.$moment.parseZone(date).format('YYYY-MM-DD') + ' at ' + this.$moment.parseZone(date).format('HH:mm')
      } else {
        return this.$moment.parseZone(date).format('YYYY-MM-DD') + ' at ' + this.$moment.parseZone(date).format('HH:mm')
      }
    },
    $roundOff: function (number) {
      return Math.round(number).toLocaleString('en')
    },
    $formatDate (data) {
      return this.$formated_date(data.created_at)
    },
    $truncateString (string, number) {
      let String = string.length
      if (String > number) {
        return string.substring(0, number) + '...'
      } else {
        return string
      }
    },
    $toolTip (divId) {
      return { 'placement': 'top-end', html: '#' + divId, 'reactive': true, 'interactive': true, 'theme': 'light', 'animateFill': false }
    }
  },
  computed: {
    screenSize () {
      return screen.width
    },
    $titles () {
      return ['Mr.', 'Mrs.', 'Ms.', 'Miss', 'Dr.', 'Prof.', 'Pst.']
    },
    $loadingStatus () {
      return this.$store.state.LoadingStore.loading
    }
  }

}
