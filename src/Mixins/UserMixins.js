export default {
  methods: {
    $editUser (userId) {
      this.$router.push({
        name: 'User',
        params: {
          user_id: userId
        }
      })
    },
    $deleteUser (userId) {
      this.$store.dispatch('delete_user', userId)
    },
    $doLogout () {
      localStorage.clear()
      window.open('/', '_self')
    }
  },
  computed: {
    $allUserTypes () {
      return this.$store.state.UserTypeStore.all_user_types
    },
    $allUsers () {
      return this.$store.state.UserStore.all_users
    },
    $loggedInUser () {
      return this.$store.state.AuthenticationStore.logged_in_user
    },
    $singleUser () {
      return this.$store.state.UserStore.user
    }
  }
}
