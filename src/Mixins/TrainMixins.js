export default {
  methods: {
    $openTrain (trainId) {
      this.$router.push({
        name: 'Train',
        params: {
          train_id: trainId
        }
      })
    }
  },
  computed: {
    $allTrains () {
      return this.$store.state.TrainStore.all_trains
    },
    $singleTrain () {
      return this.$store.state.TrainStore.train
    }
  }
}
