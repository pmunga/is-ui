// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import store from './Vuex/Store'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import axios from 'axios'

Vue.config.productionTip = false
Vue.use(Vuetify, {
  theme: {
    primary: '#00796B',
    secondary: '#FFAB00',
    // secondary: '#232d3b',
    warning: '#f38532',
    error: '#ed4a41'
  }
})
Vue.use(VueMomentJS, moment)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDyhPQ3p-isxohYBO4SREmk2-Q_XSIMhDM',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    /// / If you want to set the version, you can do so:
    // v: '3.26',
  }

  /// / If you intend to programmatically custom event listener code
  /// / (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  /// / instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  /// / you might need to turn this on.
  // autobindAllEvents: false,

  /// / If you want to manually install components, e.g.
  /// / import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  /// / Vue.component('GmapMarker', GmapMarker)
  /// / then disable the following:
  // installComponents: true,
})
Vue.mixin(require('./Mixins/Export'))

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  store.dispatch('loading_true')
  // Do something before request is sent
  config.responseType = 'json'
  if (localStorage.getItem('access_token') !== null) {
    config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('access_token')
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  store.dispatch('loading_false')
  // Do something with response data
  if (response.status < 400) {
    if (response.data.message) {
      VueNotifications.info({message: response.data.message})
    }
  }
  return response
}, function (error) {
  if (error.request.response.hasOwnProperty('message')) {
    VueNotifications.error({ message: error.request.response.message })
  } else {
    var errorKeys = Object.keys(error.request.response)
    if (errorKeys.length > 0) {
      for (var i = 0; errorKeys.length > i; i++) {
        VueNotifications.error({message: errorKeys[i] + ' - ' + error.request.response[errorKeys[i]][0]})
      }
    }
  }
  store.dispatch('loading_false')
  // Do something with response error
  if (error.request.status === 401 || error.request.status === 403) {
    localStorage.removeItem('access_token')
    store.dispatch('refresh_token')
    console.log('info', 'done query')
  } else {
    if (error.hasOwnProperty('response')) {
      if (error.request.response.hasOwnProperty('message')) {
        VueNotifications.error({message: error.request.response.message})
      }
      if (error.request.response.hasOwnProperty('messages')) {
        for (var i = 0; error.request.response.messages.length > i; i++) {
          VueNotifications.error({message: error.request.response.messages[i]})
        }
      }
    }
  }
  return Promise.reject(error)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
