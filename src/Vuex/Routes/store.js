import mutations from './mutations'
import actions from './actions'

const state = {
  all_routes: [],
  routes: [],
  route: [],
  deleted_routes: []
}

export default {
  state, mutations, actions
}
