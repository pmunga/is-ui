import axios from 'axios'
import {RouteUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_routes (context) {
    axios.get(RouteUrls.getAllRoutes).then(function (response) {
      context.commit('GET_ALL_ROUTES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_ROUTES', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_routes (context) {
    axios.get(RouteUrls.getDeletedRoutes).then(function (response) {
      context.commit('GET_DELETED_ROUTES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_ROUTES', data)
      context.dispatch('loading_false')
    })
  },
  get_route (context, publicId) {
    axios.get(RouteUrls.getRoute + publicId + '/').then(function (response) {
      context.commit('GET_ROUTE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ROUTE', data)
      context.dispatch('loading_false')
    })
  },
  post_route (context, data) {
    axios.post(RouteUrls.postRoute, data).then(function (response) {
      context.dispatch('get_all_routes')
      router.push({
        name: 'Routes'
      })
      context.dispatch('loading_false')
    })
  },
  update_route (context, data) {
    axios.patch(RouteUrls.editRoute, data).then(function (response) {
      context.dispatch('get_all_routes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_route (context, publicId) {
    axios.delete(RouteUrls.deleteRoute + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_ROUTES', response.data)
      router.push({
        name: 'Module.DeletedRoutes'
      })
      context.dispatch('loading_false')
    })
  },
  restore_route (context, publicId) {
    axios.get(RouteUrls.restoreRoute + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ROUTES', response.data)
      router.push({
        name: 'Module.Routes'
      })
      context.dispatch('loading_false')
    })
  }
}
