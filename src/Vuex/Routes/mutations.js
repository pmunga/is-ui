export default {
  GET_ROUTES (state, data) {
    state.routes = data
  },
  GET_ALL_ROUTES (state, data) {
    state.all_routes = data
  },
  GET_ROUTE (state, data) {
    state.route = data
  },
  GET_DELETED_ROUTES (state, data) {
    state.deleted_routes = data
  }
}
