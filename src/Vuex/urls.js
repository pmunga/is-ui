// If there is an external url file uncomment below and replace main url with the import
// import url from ''

const mainUrl = 'http://127.0.0.1:8000/'

export const UserTypeUrls = {
  postUsertype: mainUrl + 'user-types',
  getAllUser_types: mainUrl + 'user-types',
  getUsertype: mainUrl + 'user-types/',
  editUsertype: mainUrl + 'user-types/',
  getDeletedUser_types: mainUrl + 'user-types',
  deleteUsertype: mainUrl + 'user-types'
}

export const UserUrls = {
  postUser: mainUrl + 'users/',
  getAllUsers: mainUrl + 'users/',
  getUser: mainUrl + 'users/',
  editUser: mainUrl + 'users/',
  getDeletedUsers: mainUrl + 'users/',
  deleteUser: mainUrl + 'users/'
}

export const TrainUrls = {
  postTrain: mainUrl + 'trains/',
  getAllTrains: mainUrl + 'trains/',
  getTrain: mainUrl + 'trains/',
  editTrain: mainUrl + 'trains/',
  getDeletedTrains: mainUrl + 'trains/',
  deleteTrain: mainUrl + 'trains/'
}

export const StationUrls = {
  postStation: mainUrl + 'stations/',
  getAllStations: mainUrl + 'stations/',
  getStation: mainUrl + 'stations/',
  editStation: mainUrl + 'stations/',
  getDeletedStations: mainUrl + 'stations/',
  deleteStation: mainUrl + 'stations/'
}

export const RouteUrls = {
  postRoute: mainUrl + 'routes/',
  getAllRoutes: mainUrl + 'routes/',
  getRoute: mainUrl + 'routes/',
  editRoute: mainUrl + 'routes/',
  getDeletedRoutes: mainUrl + 'routes/',
  deleteRoute: mainUrl + 'routes/'
}

export const ScheduleUrls = {
  postSchedule: mainUrl + 'schedules/',
  searchAvailability: mainUrl + 'find-schedule',
  getAllSchedules: mainUrl + 'schedules/',
  getSchedule: mainUrl + 'schedules/',
  editSchedule: mainUrl + 'schedules/',
  getDeletedSchedules: mainUrl + 'schedules/',
  deleteSchedule: mainUrl + 'schedules/'
}

export const StopStationUrls = {
  postStopstation: mainUrl + 'routes-stops/',
  getAllStop_stations: mainUrl + 'routes-stops/',
  getStopstation: mainUrl + 'routes-stops/',
  editStopstation: mainUrl + 'routes-stops/',
  getDeletedStop_stations: mainUrl + 'routes-stops/',
  deleteStopstation: mainUrl + 'routes-stops/'
}

export const AuthenticationUrls = {
  loggedInUser: mainUrl + 'user-detail/',
  postAuthentication: mainUrl + 'api/token/',
  refreshToken: mainUrl + 'api/token/',
  getAllAuthentications: mainUrl + 'get_all',
  getAuthentication: mainUrl + 'get_single',
  editAuthentication: mainUrl + 'update',
  getDeletedAuthentications: mainUrl + 'delete',
  deleteAuthentication: mainUrl + 'delete'
}

export const BookingUrls = {
  postBooking: mainUrl + 'bookings/',
  getAllBookings: mainUrl + 'bookings/',
  getBooking: mainUrl + 'bookings/',
  editBooking: mainUrl + 'bookings/',
  getDeletedBookings: mainUrl + 'bookings/',
  deleteBooking: mainUrl + 'bookings/'
}
