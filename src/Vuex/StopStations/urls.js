// If there is an external url file uncomment below and replace main url with the import
// import url from ''

const mainUrl = 'http://../'

export const StopStationUrls = {
  postStopstation: mainUrl + 'post',
  getAllStop_stations: mainUrl + 'get_all',
  getStopstation: mainUrl + 'get_single',
  editStopstation: mainUrl + 'update',
  getDeletedStop_stations: mainUrl + 'delete',
  deleteStopstation: mainUrl + 'delete'
}
