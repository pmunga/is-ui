export default {
  GET_STOP_STATIONS (state, data) {
    state.stop_stations = data.data
  },
  GET_ALL_STOP_STATIONS (state, data) {
    state.all_stop_stations = data.data
  },
  GET_STOPSTATION (state, data) {
    state.stopstation = data.data
  },
  GET_DELETED_STOP_STATIONS (state, data) {
    state.deleted_stop_stations = data.data
  }
}
