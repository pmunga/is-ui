import axios from 'axios'
import {StopStationUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_stop_stations (context) {
    axios.get(StopStationUrls.getAllStop_stations).then(function (response) {
      context.commit('GET_ALL_STOP_STATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_STOP_STATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_stop_stations (context) {
    axios.get(StopStationUrls.getDeletedStop_stations).then(function (response) {
      context.commit('GET_DELETED_STOP_STATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_STOP_STATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_stopstation (context, publicId) {
    axios.get(StopStationUrls.getStopstation + publicId).then(function (response) {
      context.commit('GET_STOPSTATION', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_STOPSTATION', data)
      context.dispatch('loading_false')
    })
  },
  post_stopstation (context, data) {
    axios.post(StopStationUrls.postStopstation, data).then(function (response) {
      context.dispatch('get_route', data.route)
      context.dispatch('loading_false')
    })
  },
  update_stopstation (context, data) {
    axios.patch(StopStationUrls.editStopstation, data).then(function (response) {
      context.dispatch('get_route', data.route)
      context.dispatch('loading_false')
    })
  },
  delete_stopstation (context, publicId) {
    axios.delete(StopStationUrls.deleteStopstation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STOP_STATIONS', response.data)
      router.push({
        name: 'Module.DeletedStop_stations'
      })
      context.dispatch('loading_false')
    })
  },
  restore_stopstation (context, publicId) {
    axios.get(StopStationUrls.restoreStopstation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STOP_STATIONS', response.data)
      router.push({
        name: 'Module.Stop_stations'
      })
      context.dispatch('loading_false')
    })
  }
}
