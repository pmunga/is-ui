import mutations from './mutations'
import actions from './actions'

const state = {
  all_stop_stations: [],
  stop_stations: [],
  stopstation: [],
  deleted_stop_stations: []
}

export default {
  state, mutations, actions
}
