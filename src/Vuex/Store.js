import Vue from 'vue'
import Vuex from 'vuex'

import UserTypeStore from './UserTypes/store'
import UserStore from './Users/store'
import TrainStore from './Trains/store'
import StationStore from './Stations/store'
import RouteStore from './Routes/store'
import ScheduleStore from './Schedules/store'
import StopStations from './StopStations/store'
import AuthenticationStore from './Authentication/store'
import BookingStore from './Bookings/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    UserTypeStore,
    UserStore,
    TrainStore,
    StationStore,
    RouteStore,
    ScheduleStore,
    StopStations,
    AuthenticationStore,
    BookingStore
  },
  strict: debug
})
