export default {
  GET_USER_TYPES (state, data) {
    state.user_types = data.data
  },
  GET_ALL_USER_TYPES (state, data) {
    state.all_user_types = data.data
  },
  GET_USERTYPE (state, data) {
    state.usertype = data.data
  },
  GET_DELETED_USER_TYPES (state, data) {
    state.deleted_user_types = data.data
  }
}
