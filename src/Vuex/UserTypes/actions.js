import axios from 'axios'
import {UserTypeUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_user_types (context) {
    axios.get(UserTypeUrls.getAllUser_types).then(function (response) {
      context.commit('GET_ALL_USER_TYPES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_USER_TYPES', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_user_types (context) {
    axios.get(UserTypeUrls.getDeletedUser_types).then(function (response) {
      context.commit('GET_DELETED_USER_TYPES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_USER_TYPES', data)
      context.dispatch('loading_false')
    })
  },
  get_usertype (context, publicId) {
    axios.get(UserTypeUrls.getUsertype + publicId).then(function (response) {
      context.commit('GET_USERTYPE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_USERTYPE', data)
      context.dispatch('loading_false')
    })
  },
  post_usertype (context, data) {
    axios.post(UserTypeUrls.postUsertype, data).then(function (response) {
      context.dispatch('get_all_user_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  update_usertype (context, data) {
    axios.patch(UserTypeUrls.editUsertype + data.id, data).then(function (response) {
      context.dispatch('get_all_user_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_usertype (context, publicId) {
    axios.delete(UserTypeUrls.deleteUsertype + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_USER_TYPES', response.data)
      router.push({
        name: 'Module.DeletedUser_types'
      })
      context.dispatch('loading_false')
    })
  },
  restore_usertype (context, publicId) {
    axios.get(UserTypeUrls.restoreUsertype + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USER_TYPES', response.data)
      router.push({
        name: 'Module.User_types'
      })
      context.dispatch('loading_false')
    })
  }
}
