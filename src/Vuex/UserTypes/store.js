import mutations from './mutations'
import actions from './actions'

const state = {
  all_user_types: [],
  user_types: [],
  usertype: [],
  deleted_user_types: []
}

export default {
  state, mutations, actions
}
