export default {
  GET_TRAINS (state, data) {
    state.trains = data
  },
  GET_ALL_TRAINS (state, data) {
    state.all_trains = data
  },
  GET_TRAIN (state, data) {
    state.train = data
  },
  GET_DELETED_TRAINS (state, data) {
    state.deleted_trains = data
  }
}
