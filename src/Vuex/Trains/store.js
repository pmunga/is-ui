import mutations from './mutations'
import actions from './actions'

const state = {
  all_trains: [],
  trains: [],
  train: [],
  deleted_trains: []
}

export default {
  state, mutations, actions
}
