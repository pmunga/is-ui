import axios from 'axios'
import {TrainUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_trains (context) {
    axios.get(TrainUrls.getAllTrains).then(function (response) {
      context.commit('GET_ALL_TRAINS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_TRAINS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_trains (context) {
    axios.get(TrainUrls.getDeletedTrains).then(function (response) {
      context.commit('GET_DELETED_TRAINS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_TRAINS', data)
      context.dispatch('loading_false')
    })
  },
  get_train (context, publicId) {
    axios.get(TrainUrls.getTrain + publicId + '/').then(function (response) {
      context.commit('GET_TRAIN', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_TRAIN', data)
      context.dispatch('loading_false')
    })
  },
  post_train (context, data) {
    axios.post(TrainUrls.postTrain, data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(function (response) {
      context.dispatch('get_all_trains')
      router.push({
        name: 'Trains'
      })
      context.dispatch('loading_false')
    })
  },
  update_train (context, data) {
    axios.patch(TrainUrls.editTrain, data).then(function (response) {
      context.dispatch('get_all_trains')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_train (context, publicId) {
    axios.delete(TrainUrls.deleteTrain + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_TRAINS', response.data)
      router.push({
        name: 'Module.DeletedTrains'
      })
      context.dispatch('loading_false')
    })
  },
  restore_train (context, publicId) {
    axios.get(TrainUrls.restoreTrain + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_TRAINS', response.data)
      router.push({
        name: 'Module.Trains'
      })
      context.dispatch('loading_false')
    })
  }
}
