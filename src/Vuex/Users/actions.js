import axios from 'axios'
import {UserUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_users (context) {
    axios.get(UserUrls.getAllUsers).then(function (response) {
      context.commit('GET_ALL_USERS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_USERS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_users (context) {
    axios.get(UserUrls.getDeletedUsers).then(function (response) {
      context.commit('GET_DELETED_USERS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_USERS', data)
      context.dispatch('loading_false')
    })
  },
  get_user (context, publicId) {
    axios.get(UserUrls.getUser + publicId).then(function (response) {
      context.commit('GET_USER', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_USER', data)
      context.dispatch('loading_false')
    })
  },
  post_user (context, data) {
    axios.post(UserUrls.postUser, data).then(function (response) {
      context.dispatch('get_all_users')
      router.push({
        name: 'Users'
      })
      context.dispatch('loading_false')
    })
  },
  update_user (context, data) {
    axios.patch(UserUrls.editUser + '/', data).then(function (response) {
      context.dispatch('get_all_users')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_user (context, publicId) {
    axios.delete(UserUrls.deleteUser + publicId + '/').then(function (response) {
      context.dispatch('get_all_users')
      context.dispatch('loading_false')
    })
  },
  restore_user (context, publicId) {
    axios.get(UserUrls.restoreUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USERS', response.data)
      router.push({
        name: 'Module.Users'
      })
      context.dispatch('loading_false')
    })
  }
}
