export default {
  GET_USERS (state, data) {
    state.users = data
  },
  GET_ALL_USERS (state, data) {
    state.all_users = data
  },
  GET_USER (state, data) {
    state.user = data
  },
  GET_DELETED_USERS (state, data) {
    state.deleted_users = data
  }
}
