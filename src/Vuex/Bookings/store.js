import mutations from './mutations'
import actions from './actions'

const state = {
  all_bookings: [],
  bookings: [],
  booking: [],
  deleted_bookings: []
}

export default {
  state, mutations, actions
}