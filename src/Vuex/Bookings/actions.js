import axios from 'axios'
import {BookingUrls} from './../urls'
import router from '../../router/index'

export default {
  get_all_bookings (context) {
    axios.get(BookingUrls.getAllBookings).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_BOOKINGS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_bookings (context) {
    axios.get(BookingUrls.getDeletedBookings).then(function (response) {
      context.commit('GET_DELETED_BOOKINGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_BOOKINGS', data)
      context.dispatch('loading_false')
    })
  },
  get_booking (context, publicId) {
    axios.get(BookingUrls.getBooking + publicId).then(function (response) {
      context.commit('GET_BOOKING', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_BOOKING', data)
      context.dispatch('loading_false')
    })
  },
  post_booking (context, data) {
    axios.post(BookingUrls.postBooking, data).then(function (response) {
      context.dispatch('get_all_bookings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  buy_ticket (context, data) {
    axios.post(BookingUrls.postBooking, data).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  update_booking (context, data) {
    axios.patch(BookingUrls.editBooking, data).then(function (response) {
      context.dispatch('get_all_bookings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
   })
  },
  delete_booking (context, publicId) {
    axios.delete(BookingUrls.deleteBooking + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_BOOKINGS', response.data)
      router.push({
        name: 'Module.DeletedBookings'
      })
      context.dispatch('loading_false')
    })
  },
  restore_booking (context, publicId) {
    axios.get(BookingUrls.restoreBooking + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_BOOKINGS', response.data)
      router.push({
        name: 'Module.Bookings'
      })
      context.dispatch('loading_false')
    })
  }
}
