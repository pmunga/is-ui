export default {
  GET_BOOKINGS (state, data) {
    state.bookings = data.data
  },
  GET_ALL_BOOKINGS (state, data) {
    state.all_bookings = data.data
  },
  GET_BOOKING (state, data) {
    state.booking = data.data
  },
  GET_DELETED_BOOKINGS (state, data) {
    state.deleted_bookings = data.data
  }
}
