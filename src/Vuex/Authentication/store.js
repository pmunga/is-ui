import mutations from './mutations'
import actions from './actions'

const state = {
  access_token: [],
  refresh_token: [],
  logged_in_user: []
}

export default {
  state, mutations, actions
}
