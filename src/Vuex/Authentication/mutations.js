export default {
  GET_TOKENS (state, data) {
    state.access_token = data.access
    localStorage.setItem('access_token', data.access)
    state.refresh_token = data.refresh
    localStorage.setItem('refresh_token', data.refresh)
  },
  USER_DETAILS (state, data) {
    state.logged_in_user = data.data
  },
  ACCESS_TOKENS (state, data) {
    state.access_token = data.access
    localStorage.setItem('access_token', data.access)
  }
}
