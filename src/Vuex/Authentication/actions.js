import axios from 'axios'
import {AuthenticationUrls} from '../urls'
import router from '../../router/index'

export default {
  post_authentication (context, data) {
    axios.post(AuthenticationUrls.postAuthentication, data).then(function (response) {
      context.commit('GET_TOKENS', response.data)
      context.dispatch('get_user_details')
    })
  },
  get_user_details (context) {
    axios.get(AuthenticationUrls.loggedInUser).then(function (response) {
      context.commit('USER_DETAILS', response.data)
      context.dispatch('loading_false')
      router.push({
        name: 'Users'
      })
    })
  },
  refresh_token (context) {
    axios.post(AuthenticationUrls.refreshToken, {refresh: localStorage.getItem('refresh_token')}).then(function (response) {
      context.commit('ACCESS_TOKENS', response.data)
      context.dispatch('loading_false')
      router.push({
        name: 'Users'
      })
    })
  }
}
