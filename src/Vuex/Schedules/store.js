import mutations from './mutations'
import actions from './actions'

const state = {
  all_schedules: [],
  schedules: [],
  schedule: [],
  deleted_schedules: [],
  search_results: []
}

export default {
  state, mutations, actions
}
