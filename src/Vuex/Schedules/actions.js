import axios from 'axios'
import {ScheduleUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_schedules (context) {
    axios.get(ScheduleUrls.getAllSchedules).then(function (response) {
      context.commit('GET_ALL_SCHEDULES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_SCHEDULES', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_schedules (context) {
    axios.get(ScheduleUrls.getDeletedSchedules).then(function (response) {
      context.commit('GET_DELETED_SCHEDULES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_SCHEDULES', data)
      context.dispatch('loading_false')
    })
  },
  get_schedule (context, publicId) {
    axios.get(ScheduleUrls.getSchedule + publicId).then(function (response) {
      context.commit('GET_SCHEDULE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_SCHEDULE', data)
      context.dispatch('loading_false')
    })
  },
  post_schedule (context, data) {
    axios.post(ScheduleUrls.postSchedule, data).then(function (response) {
      context.dispatch('get_all_schedules')
      router.push({
        name: 'UpcomingSchedules'
      })
      context.dispatch('loading_false')
    })
  },
  find_availability (context, data) {
    axios.post(ScheduleUrls.searchAvailability, data).then(function (response) {
      context.commit('SEARCH_RESULTS', response.data)
      router.push({
        name: 'HomeSchedules'
      })
      context.dispatch('loading_false')
    })
  },
  update_schedule (context, data) {
    axios.patch(ScheduleUrls.editSchedule, data).then(function (response) {
      context.dispatch('get_all_schedules')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_schedule (context, publicId) {
    axios.delete(ScheduleUrls.deleteSchedule + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_SCHEDULES', response.data)
      router.push({
        name: 'Module.DeletedSchedules'
      })
      context.dispatch('loading_false')
    })
  },
  restore_schedule (context, publicId) {
    axios.get(ScheduleUrls.restoreSchedule + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_SCHEDULES', response.data)
      router.push({
        name: 'Module.Schedules'
      })
      context.dispatch('loading_false')
    })
  }
}
