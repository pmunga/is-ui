export default {
  GET_SCHEDULES (state, data) {
    state.schedules = data
  },
  GET_ALL_SCHEDULES (state, data) {
    state.all_schedules = data
  },
  GET_SCHEDULE (state, data) {
    state.schedule = data
  },
  SEARCH_RESULTS (state, data) {
    state.search_results = data.data
  },
  GET_DELETED_SCHEDULES (state, data) {
    state.deleted_schedules = data
  }
}
