import axios from 'axios'
import {StationUrls} from '../urls'
import router from '../../router/index'

export default {
  get_all_stations (context) {
    axios.get(StationUrls.getAllStations).then(function (response) {
      context.commit('GET_ALL_STATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_STATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_stations (context) {
    axios.get(StationUrls.getDeletedStations).then(function (response) {
      context.commit('GET_DELETED_STATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_STATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_station (context, publicId) {
    axios.get(StationUrls.getStation + publicId + '/').then(function (response) {
      context.commit('GET_STATION', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_STATION', data)
      context.dispatch('loading_false')
    })
  },
  post_station (context, data) {
    axios.post(StationUrls.postStation, data).then(function (response) {
      context.dispatch('get_all_stations')
      router.push({
        name: 'Stations'
      })
      context.dispatch('loading_false')
    })
  },
  update_station (context, data) {
    axios.patch(StationUrls.editStation, data).then(function (response) {
      context.dispatch('get_all_stations')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_station (context, publicId) {
    axios.delete(StationUrls.deleteStation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STATIONS', response.data)
      router.push({
        name: 'Module.DeletedStations'
      })
      context.dispatch('loading_false')
    })
  },
  restore_station (context, publicId) {
    axios.get(StationUrls.restoreStation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STATIONS', response.data)
      router.push({
        name: 'Module.Stations'
      })
      context.dispatch('loading_false')
    })
  }
}
