export default {
  GET_STATIONS (state, data) {
    state.stations = data
  },
  GET_ALL_STATIONS (state, data) {
    state.all_stations = data
  },
  GET_STATION (state, data) {
    state.station = data
  },
  GET_DELETED_STATIONS (state, data) {
    state.deleted_stations = data
  }
}
