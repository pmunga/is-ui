import mutations from './mutations'
import actions from './actions'

const state = {
  all_stations: [],
  stations: [],
  station: [],
  deleted_stations: []
}

export default {
  state, mutations, actions
}
