import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Modules/Home'
import Index from '@/components/Home/Index'
import HomeSchedules from '@/components/Home/HomeSchedules'
import Login from '@/components/Home/Login'

import Admin from '@/components/Modules/Admin'
import NewUser from '@/components/Admin/Users/NewUser'
import User from '@/components/Admin/Users/User'
import Users from '@/components/Admin/Users/Users'
import UserSettings from '@/components/Admin/Users/UserSettings'
import AddTrain from '@/components/Admin/Trains/AddTrain'
import Trains from '@/components/Admin/Trains/Trains'
import Train from '@/components/Admin/Trains/Train'
import Stations from '@/components/Admin/Stations/Stations'
import AddStation from '@/components/Admin/Stations/AddStation'
import Station from '@/components/Admin/Stations/Station'
import AddSchedule from '@/components/Admin/Schedules/AddSchedule'
import ScheduleHistory from '@/components/Admin/Schedules/ScheduleHistory'
import ScheduleSettings from '@/components/Admin/Schedules/ScheduleSettings'
import UpcomingSchedules from '@/components/Admin/Schedules/UpcomingSchedules'
import AddRoute from '@/components/Admin/Routes/AddRoute'
import Route from '@/components/Admin/Routes/Route'
import Routes from '@/components/Admin/Routes/Routes'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '',
          component: Index,
          name: 'Index'
        },
        {
          path: 'schedules',
          component: HomeSchedules,
          name: 'HomeSchedules'
        },
        {
          path: 'login',
          component: Login,
          name: 'Login'
        }
      ]
    },
    {
      path: '/m',
      component: Admin,
      children: [
        {
          path: 'new-user',
          component: NewUser,
          name: 'NewUser'
        },
        {
          path: 'user/:user_id',
          component: User,
          name: 'User'
        },
        {
          path: 'users',
          component: Users,
          name: 'Users'
        },
        {
          path: 'user-settings',
          component: UserSettings,
          name: 'UserSettings'
        },
        // Trains
        {
          path: 'add-train',
          component: AddTrain,
          name: 'AddTrain'
        },
        {
          path: 'trains',
          component: Trains,
          name: 'Trains'
        },
        {
          path: 'train/:train_id',
          component: Train,
          name: 'Train'
        },
        // Stations
        {
          path: 'station/:station_id',
          component: Station,
          name: 'Station'
        },
        {
          path: 'add-station',
          component: AddStation,
          name: 'AddStation'
        },
        {
          path: 'all-stations',
          component: Stations,
          name: 'Stations'
        },
        // Schedules
        {
          path: 'add-schedule',
          component: AddSchedule,
          name: 'AddSchedule'
        },
        {
          path: 'schedule-history',
          component: ScheduleHistory,
          name: 'ScheduleHistory'
        },
        {
          path: 'schedule-settings',
          component: ScheduleSettings,
          name: 'ScheduleSettings'
        },
        {
          path: 'upcoming-schedules',
          component: UpcomingSchedules,
          name: 'UpcomingSchedules'
        },
        // Routes
        {
          path: 'add-route',
          component: AddRoute,
          name: 'AddRoute'
        },
        {
          path: 'routes',
          component: Routes,
          name: 'Routes'
        },
        {
          path: 'route/:route_id',
          component: Route,
          name: 'Route'
        }
      ]
    }
  ],
  mode: 'history'
})
